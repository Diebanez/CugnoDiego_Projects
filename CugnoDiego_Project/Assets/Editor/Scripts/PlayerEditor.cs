﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(Player))]
public class PlayerEditor : Editor {
    ReorderableList NodesList;
    Player player
    {
        get
        {
            return target as Player;
        }
    }

    private void OnEnable()
    {
        serializedObject.Update();
        NodesList = new ReorderableList(serializedObject, serializedObject.FindProperty("Nodes"), true, true, true, true);
        NodesList.drawElementCallback += OnDrawElement;
        NodesList.onAddCallback += OnAddItemToList;
        NodesList.onRemoveCallback += OnRemoveItemFromList;
    }
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        serializedObject.Update();
        NodesList.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }

    void OnAddItemToList(ReorderableList list)
    {
        Vector3 newNode = new Vector3(0, 0, 0);
        player.Nodes.Add(newNode);
    }

    void OnRemoveItemFromList(ReorderableList list)
    {
        player.Nodes.RemoveAt(list.index);
    }

    void OnDrawElement(Rect rect, int index, bool active, bool focused)
    {
        Vector3 item = player.Nodes[index];
        EditorGUI.BeginChangeCheck();
        item = EditorGUI.Vector3Field(rect, "Position",item);
        if (EditorGUI.EndChangeCheck())
        {
            RaycastHit hit;
            if(Physics.Raycast(item, -Vector3.up, out hit, 100))
            {
                item = new Vector3(item.x, item.y - hit.distance, item.z);
            }
            EditorUtility.SetDirty(target);
        }
    }

    private void OnSceneGUI()
    {
        Handles.color = Color.cyan;
        if (player.Nodes.Count > 1)
        {
            for (int i = 0; i < player.Nodes.Count - 1; i++)
            {
                Handles.DrawLine(player.Nodes[i], player.Nodes[i + 1]);
            }
            Handles.DrawLine(player.Nodes[player.Nodes.Count - 1], player.Nodes[0]);
        }

        for (int i = 0; i < player.Nodes.Count; i++) { 
            if (!Application.isPlaying)
            {
                player.Nodes[i] = Handles.DoPositionHandle(player.Nodes[i], Quaternion.identity);
                Vector3 RelativeDirection = player.Nodes[i] - Camera.current.transform.position;
                RaycastHit hit;
                if (Physics.Raycast(Camera.current.transform.position, RelativeDirection, out hit))
                {
                    player.Nodes[i] = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                    Repaint();
                }
            }
        }

    }

}
