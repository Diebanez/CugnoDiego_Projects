﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LoadFromAssetBundle : MonoBehaviour {
    [SerializeField]
    string Bundle;
    [SerializeField]
    string PrefabToLoad;
    [SerializeField]
    string Server;

    UnityWebRequest Request;

    private void Start()
    {
        DataManager.instance.LoadBundleFromNetwork(
            Server + "/" + Bundle, 
            (result, obj) =>
            {
                if (result)
                {
                    Instantiate(obj, Vector3.zero, Quaternion.identity);
                }
            }, 
            PrefabToLoad);
    }

    IEnumerator InstantiateFromNetwork()
    {
        Request = UnityWebRequest.GetAssetBundle(Server + "/" + Bundle, 0);
        yield return Request.SendWebRequest();
        AssetBundle ReceiveBundle = DownloadHandlerAssetBundle.GetContent(Request);
        GameObject prefab = ReceiveBundle.LoadAsset<GameObject>(PrefabToLoad);
        Instantiate(prefab);
    }
    

}

