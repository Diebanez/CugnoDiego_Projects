﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    [SerializeField] float MovementSpeed = 1.0f;

    public List<Vector3> Nodes;
    int ActualIndex = 1;
    int OldIndex = 0;
    float timer = 0;
   

    private void Update()
    {
        timer += Time.deltaTime;
        if(transform.position == Nodes[ActualIndex]){
            OldIndex = ActualIndex;
            ActualIndex++;
            if(ActualIndex >= Nodes.Count)
            {
                ActualIndex = 0;                
            }
            timer = 0;
        }
        transform.position = Vector3.Lerp(Nodes[OldIndex], Nodes[ActualIndex], MovementSpeed * timer);
    }
}
