﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
public class DataManager : Singleton<DataManager> {    

    public void LoadBundleFromNetwork(string Uri, Action<bool, GameObject> ObjLoaded, string prefab)
    {
        StartCoroutine(InstantiateFromNetwork(Uri, ObjLoaded, prefab));
    }

    IEnumerator InstantiateFromNetwork(string Uri, Action<bool, GameObject> ObjLoaded, string prefab)
    {
        UnityWebRequest request = UnityWebRequest.GetAssetBundle(Uri, 0);
        yield return request.SendWebRequest();
        AssetBundle ReceiveBundle = DownloadHandlerAssetBundle.GetContent(request);
        GameObject result = ReceiveBundle.LoadAsset<GameObject>(prefab);
        if (result)
        {
            ObjLoaded(true, result);
        }
        else
        {
            ObjLoaded(false, result);
        }
    }
}
