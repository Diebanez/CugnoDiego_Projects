﻿Shader "Custom/Lambert" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NorTex("Normal Map", 2D ) = "white"{}
		_SpecTex("Specular (RGB)", 2D) = "white"{}
		_Shininess("Shininess", Range(0.01, 1)) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NorTex;
		sampler2D _SpecTex;
		fixed _Shininess;
		half4 _GlobalSpecularPos;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NorTex;
			float3 worldRefl;
			float3 viewDir;
			float3 worldPos;
			INTERNAL_DATA
		};
		
		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			//Albedo calculation
			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);

			//Normal calculation
			fixed3 nor = UnpackNormal(tex2D(_NorTex, IN.uv_NorTex));

			//Specular calculation
			fixed4 spec = tex2D(_SpecTex, IN.uv_MainTex);
			fixed3 specLightDir = normalize(_GlobalSpecularPos.xyz - IN.worldPos);
			fixed3 specViewDir = normalize(_WorldSpaceCameraPos.xyz - IN.worldPos);
			half3 reflection = normalize(2 * ((dot(nor, specLightDir))) * nor - specLightDir);
			half3 specular = pow(dot(reflection, specViewDir), 1);
			specular = specular * spec.rgb;

			//Final application
			o.Albedo = tex.rgb + specular;
			o.Normal = nor;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
