﻿Shader "Custom/ElectricityShader"
{
	Properties
	{		
		[HDR]
		_Tint("Color", Color) = (1, 1, 1, 1)
		_Dissolve("Dissolve", Range(0, 1)) = 0.25
		_NoiseTex("Noise", 2D) = "gray" {}
		_GradientTex("Gradient", 2D) = "gray" {}
		_EffectOffset("Offset", Range(0,1)) = .5
		
	}
		SubShader
	{
		Pass
		{
			Tags{
				"IgnoreProjector" = "True"
				"Queue" = "Transparent"
				"RenderType" = "Transparent"
			}
			Blend One One

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"

			float4 _Tint;
			float _Dissolve;
			sampler2D _NoiseTex;
			float4 _NoiseTex_ST;
			sampler2D _GradientTex;
			float4 _GradientTex_ST;
			float _EffectOffset;

			struct vInput {
				float2 uv : TEXCOORD0;
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct vOutput {
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			float remap(float value, float low1, float high1, float low2, float high2) {
				return low2 + (value - low1) * (high2 - low2) / (high1 - low1);
			}

			vOutput vert(vInput v) {
				vOutput o;

				o.position.xyz = v.vertex.xyz + v.normal * _EffectOffset;
				o.position.w = v.vertex.w;
				o.position = UnityObjectToClipPos(o.position);
				o.uv = v.uv;

				return o;
			}

			float4 frag(vOutput i) : COLOR{
				float2 uv_NoiseOne = i.uv + mul(float2(0.2, 0.2), _Time.y);
				float2 uv_NoiseTwo = i.uv + mul(float2(-0.2, 0.05), _Time.y);

				float firstNoise = tex2D(_NoiseTex, uv_NoiseOne).r;
				float secondNoise = tex2D(_NoiseTex, uv_NoiseTwo).r;

				float dissolveRemap = remap(_Dissolve, 0, 1, -0.65, 0.65);

				float finalNoise = mul((dissolveRemap + firstNoise), (dissolveRemap + secondNoise));

				float finalNoiseRemap = remap(finalNoise, 0, 1, -10, 10);

				float finalNoiseClamp = clamp(finalNoiseRemap, 0, 1);

				float2 gradientCoords = float2(1 - finalNoiseClamp, 0);

				float4 col = tex2D(_GradientTex, gradientCoords);

				return col * _Tint;
			}			
			
			ENDCG
		}
	}
}
