﻿Shader "Custom/RampCellShader"
{
	Properties
	{
		_Tint("Color", Color) = (1, 1, 1, 1)
		_MainTex("Texture", 2D) = "white" {}
		_RampTex("Ramp Texture", 2D) = "gray" {}
		_OutlineColor("Outline Color", Color) = (0, 0, 0, 0)
		_OutlineThickness("Outline Thickness", Range(0, 1)) = 0.25
	}
	SubShader
	{
		Pass{
			Cull Front
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			float4 _OutlineColor;
			float _OutlineThickness;

			struct vInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texCoord : TEXCOORD0;
			};

			struct vOutput {
				float4 position : SV_POSITION;
			};

			vOutput vert(vInput v) {
				vOutput o;
				o.position.xyz = v.vertex + v.normal * _OutlineThickness * 0.1;
				o.position = UnityObjectToClipPos(o.position);

				return o;
			}

			float4 frag(vOutput i) : COLOR{
				return _OutlineColor;
			}

				ENDCG
		}

		Pass{
			Cull Back
			Tags{ "LightMode" = "ForwardBase" }
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			float4 _Tint;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _RampTex;
			float4 _RampTex_ST;
			uniform float4 _LightColor0;

			struct vInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texCoord : TEXCOORD0;
			};

			struct vOutput {
				float4 position : SV_POSITION;
				float3 normalDir : TEXCOORD1;
				float3 lightDir : TEXCOORD2;
				float2 uv : TEXCOORD0;
			};

			vOutput vert(vInput v) {
				vOutput o;

				//normal direction
				o.normalDir = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);

				//world position
				float4 posWorld = mul(unity_ObjectToWorld, v.vertex);

				//light direction
				float3 fragmentToLightSource = (_WorldSpaceCameraPos.xyz - posWorld.xyz);

				o.lightDir = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - posWorld.xyz, _WorldSpaceLightPos0.w));

				o.position = UnityObjectToClipPos(v.vertex);


				o.uv = v.texCoord;
				TRANSFER_VERTEX_TO_FRAGMENT(o);

				return o;
			}

			float4 frag(vOutput i) : COLOR {
				float NDotL = dot(i.normalDir, i.lightDir);
				float2 RampCoord = float2(clamp(NDotL, 0, 1), 0);

				float directDiffuse = max(0.0, NDotL) * _LightColor0.xyz;

				float3 indirectDiffuse = float3(0, 0, 0);
				indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb;

				float4 col = float4(1,1,1,1);

				float3 diffuseColor = tex2D(_MainTex, i.uv).rgb * tex2D(_RampTex, RampCoord);
				col.rgb = (directDiffuse + indirectDiffuse) * diffuseColor * _LightColor0;

				return col * _Tint;
			}

			ENDCG
		}		

		Pass{
			Cull Back
			Tags{ "LightMode" = "ForwardAdd" }
			Blend One One
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			float4 _Tint;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _RampTex;
			float4 _RampTex_ST;
			uniform float4 _LightColor0;

			struct vInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texCoord : TEXCOORD0;
			};

			struct vOutput {
				float4 position : SV_POSITION;
				float3 normalDir : TEXCOORD1;
				float3 lightDir : TEXCOORD2;
				float2 uv : TEXCOORD0;
			};

			vOutput vert(vInput v) {
				vOutput o;

				//normal direction
				o.normalDir = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);

				//world position
				float4 posWorld = mul(unity_ObjectToWorld, v.vertex);

				//light direction
				float3 fragmentToLightSource = (_WorldSpaceCameraPos.xyz - posWorld.xyz);				

				o.lightDir = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - posWorld.xyz, _WorldSpaceLightPos0.w));

				o.position = UnityObjectToClipPos(v.vertex);


				o.uv = v.texCoord;
				TRANSFER_VERTEX_TO_FRAGMENT(o);

				return o;
			}

			float4 frag(vOutput i) : COLOR{
				float NDotL = dot(i.normalDir, i.lightDir);
			float2 RampCoord = float2(clamp(NDotL, 0, 1), 0);

			float directDiffuse = max(0.0, NDotL) * _LightColor0.xyz;

			float3 indirectDiffuse = float3(0, 0, 0);
			indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb;

			float4 col = float4(1,1,1,1);

			float3 diffuseColor = tex2D(_MainTex, i.uv).rgb * tex2D(_RampTex, RampCoord);
			col.rgb = (directDiffuse + indirectDiffuse) * diffuseColor * _LightColor0;

			return col * _Tint;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}
