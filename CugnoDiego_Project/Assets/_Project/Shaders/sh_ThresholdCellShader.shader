﻿Shader "Custom/ThresholdCellShader"
{
	Properties
	{
		_Tint("Color", Color) = (1, 1, 1, 1)
		_MainTex("Texture", 2D) = "white" {}
		_LightThreshold("Light Threshold", Float) = 0.5
		_OutlineColor("Outline Color", Color) = (0, 0, 0, 0)
		_OutlineThickness("Outline Thickness", Range(0, 1)) = 0.25
	}
	SubShader
	{
		Pass{
			Cull Front
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			float4 _OutlineColor;
			float _OutlineThickness;

			struct vInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texCoord : TEXCOORD0;
			};

			struct vOutput {
				float4 position : SV_POSITION;
			};

			vOutput vert(vInput v) {
				vOutput o;
				o.position.xyz = v.vertex + v.normal * _OutlineThickness * 0.1;
				o.position = UnityObjectToClipPos(o.position);

				return o;
			}

			float4 frag(vOutput i) : COLOR{
				return _OutlineColor;
			}

			ENDCG
		}

		Pass{
			Cull Back
			Tags{ "LightMode" = "ForwardBase" }
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			float4 _Tint;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _LightThreshold;

			struct vInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texCoord : TEXCOORD0;
			};

			struct vOutput {
				float4 position : SV_POSITION;
				float3 normalDir : TEXCOORD1;
				float4 lightDir : TEXCOORD2;
				float2 uv : TEXCOORD0;
			};

			vOutput vert(vInput v) {
				vOutput o;

				//normal direction
				o.normalDir = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);

				//world position
				float4 posWorld = mul(unity_ObjectToWorld, v.vertex);

				//light direction
				float3 fragmentToLightSource = (_WorldSpaceCameraPos.xyz - posWorld.xyz);
				o.lightDir = float4(normalize(lerp(_WorldSpaceLightPos0.xyz, fragmentToLightSource, _WorldSpaceLightPos0.w)), lerp(1.0, 1.0 / length(fragmentToLightSource), _WorldSpaceLightPos0.w));

				o.position = UnityObjectToClipPos(v.vertex);

				o.uv = v.texCoord;

				return o;
			}

			float4 frag(vOutput i) : COLOR{
				float NDotL = dot(i.normalDir, i.lightDir);
				float4 col = float4(1,1,1,1);
				col.rgb = step(-NDotL, -_LightThreshold);
				return col * _Tint;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}
